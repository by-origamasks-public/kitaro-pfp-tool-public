/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'ipfs.io',
      },
      {
        protocol: 'https',
        hostname: 'nftstorage.link',
      },
      {
        protocol: 'https',
        hostname: 'cloudflare-ipfs.com',
      },
      {
        protocol: 'https',
        hostname: 'nftstorage.link',
      }
      
    ],
  },
  env: {
    SMART_CONTRACT_ADDRESS: process.env.SMART_CONTRACT_ADDRESS,
    PROJECT_NAME: process.env.PROJECT_NAME,
    ALCHEMY_KEY: process.env.ALCHEMY_KEY,
    TOKEN_NAME: process.env.TOKEN_NAME,
    BASE_IMAGE_URL: process.env.BASE_IMAGE_URL
  }
}

module.exports = nextConfig
