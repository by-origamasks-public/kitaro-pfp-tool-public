

function BlockNotHave(props) {
    return (
        <>
            <div className="h-screen items-center justify-center flex flex-col px-2 w-full">

                <div className="md:text-4xl uppercase text-xl flex font-ubuntumono text-white text-center tracking-wider">No {process.env.PROJECT_NAME} NFT in this wallet</div>
            </div>
        </>
    )
}

export default BlockNotHave;