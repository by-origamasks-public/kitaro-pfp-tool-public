import Image from 'next/image'

function BlockConnectWallet(props) {
  return (
    <>
      <div className="h-screen w-full items-center px-2 flex flex-col justify-center">

        <div className="font-ubuntumono uppercase md:text-4xl flex text-xl text-white text-center tracking-wider">
          Please connect your wallet
        </div>
      </div>
    </>
  );
}

export default BlockConnectWallet;
