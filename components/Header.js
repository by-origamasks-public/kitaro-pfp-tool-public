import { ConnectButton } from "@rainbow-me/rainbowkit";
import Image from "next/image";

function Header(props) {
  const absolute = props.absolute;
  return (
    <>
      <div className={`py-3 px-2 ${absolute ? 'absolute w-full' : ''}`} style={{ height: `${props.height}px` }}>
        <div className="flex py-2  justify-between items-center px-2">
          <div className="flex w-32 sm:w-36 items-center justify-center border border-black">
            <Image
              // loader={myLoader}
              src="/Kitaro White_Logo.png"
              alt={process.env.PROJECT_NAME}
              width="335px"
              height="94px"
              objectFit="cover"
            />
          </div>
          <div
          >
            <ConnectButton chainStatus="icon" />
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;
