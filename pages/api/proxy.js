const request = require("request");

export default (req, res) => {
  // filename only
  const fileName = `${req.query.tokenId}.${req.query.extension}`;

  const url = req.query.url;

  // set header
  res.setHeader("content-disposition", "attachment; filename=" + fileName);

  // send request to the original file
  request
    .get(url)
    .on("error", function (err) {
      res.writeHead(404, { "Content-Type": "text/html" });
      res.write("<h1>404 not found</h1>");
      res.end();
      return;
    })
    .pipe(res); // pipe converted image to HTTP response
};
