import React, { useState, useEffect } from "react";
import Head from "next/head";
import Header from "../components/Header";
import BlockPFP from "../components/Block/BlockPFP";
import BlockConnectWallet from "../components/Block/BlockConnectWallet";

import { Network, Alchemy } from "alchemy-sdk";
import { useAccount, isConnected } from "wagmi";

export default function Success(props) {
  const settings = {
    apiKey: process.env.ALCHEMY_KEY, // Replace with your Alchemy API Key.
    network: Network.ETH_MAINNET, // Replace with your network.
  };
  const alchemy = new Alchemy(settings);
  const { address, isConnected } = useAccount();

  const [downloadType, setDownloadType] = React.useState("IMAGE");
  const [tokenIds, setTokenIds] = React.useState([]);
  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    if (isConnected && address && address.length > 0) {
      alchemy.nft
        .getNftsForOwner(address, {
          contractAddresses: [
            "0x7DF64F69544c5bf71171dc5ab69b8602C2FF1E27",
          ] /*pageKey: 'MHhmMTMyZjJjOGYxZWVkZTI3MDcwZTA4NTA3NzU0MzZhMGU2ZTcyNjhhOjB4MDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDliODpmYWxzZQ=='*/,
        })
        .then((res) => {
          setTokenIds(res.ownedNfts);
          setLoading(false);
        });
    }
  }, [address]);

  return (
    <>
      <Head>
        <title>Kitaro World Assets</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="favicon/apple-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="favicon/apple-icon-60x60.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="favicon/apple-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="favicon/apple-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href="favicon/apple-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href="favicon/apple-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="favicon/apple-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="favicon/apple-icon-152x152.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="favicon/apple-icon-180x180.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="favicon/android-icon-192x192.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="96x96"
          href="favicon/favicon-96x96.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="favicon/favicon-16x16.png"
        />
        <link rel="manifest" href="favicon/manifest.json" />
        <meta name="msapplication-TileColor" content="#ffffff" />
        <meta
          name="msapplication-TileImage"
          content="favicon/ms-icon-144x144.png"
        />
        <meta name="theme-color" content="#ffffff"></meta>
      </Head>
      <div className="w-full flex justify-center bg-black">
        <Header absolute={true} />

        {isConnected ? (
          <BlockPFP tokenIds={tokenIds} isLoading={loading} />
        ) : (
          <BlockConnectWallet />
        )}
      </div>
    </>
  );
}
