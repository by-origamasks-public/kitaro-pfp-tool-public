import "../styles/globals.css";
import Head from "next/head";
import {
  RainbowKitProvider,
  lightTheme,
  AvatarComponent,
} from "@rainbow-me/rainbowkit";
import { mainnet, optimism } from "wagmi/chains";
import { chain, configureChains, createClient, WagmiConfig } from "wagmi";
import { alchemyProvider } from "wagmi/providers/alchemy";

import { connectorsForWallets } from "@rainbow-me/rainbowkit";
import {
  metaMaskWallet,
  walletConnectWallet,
} from "@rainbow-me/rainbowkit/wallets";
import "@rainbow-me/rainbowkit/styles.css";

import { createTheme, ThemeProvider } from "@mui/material";

const theme = createTheme({
  palette: {
    type: "light",
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FFFFFF",
    },
    putih: {
      main: "#ffffff",
      contrastText: "#ffffff",
      disabledBackground: "#0000FF",
      disabled: "#FFFFFF",
    },
  },
});

function MyApp({ Component, pageProps }) {
  const { chains, provider } = configureChains(
    [mainnet],
    [alchemyProvider({ apiKey: process.env.ALCHEMY_KEY })]
  );

  const connectors = connectorsForWallets([
    {
      groupName: "Recommended",
      wallets: [
        metaMaskWallet({
          chains,
        }),
        walletConnectWallet({ chains }),
        // rainbowWallet({ chains }),
      ],
    },
  ]);

  const wagmiClient = createClient({
    autoConnect: true,
    connectors,
    provider,
    // webSocketProvider,
  });

  return (
    <WagmiConfig client={wagmiClient}>
      <RainbowKitProvider chains={chains} coolMode>
        <ThemeProvider theme={theme}>
          <Head>
            <title>{process.env.PROJECT_NAME}</title>
          </Head>
          <Component {...pageProps} />
        </ThemeProvider>
      </RainbowKitProvider>
    </WagmiConfig>
  );
}

export default MyApp;
