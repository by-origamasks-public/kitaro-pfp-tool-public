/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      screens: {
        'sm': '414px',
      },
      fontFamily: {
        'nekolina': ['Nekolina', 'serif'],
        'ubuntumono': ['Ubuntu Mono', 'monospace'], 
      },
      colors: {

      },
      rotate: {
        '14': '14deg',
      }
    },
  },
  plugins: [],
}
