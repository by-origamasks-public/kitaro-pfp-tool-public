## IMPORTANT THING ON CONNECT WALLET SECURELY

Check these files to verify on how secure you'll connect wallet

- components/Block/BlockConnectWallet.js
- components/Header.js
- pages/\_app.js
- pages/index.js

```bash
Tech stack:
getNfts : Alchemy SDK (https://docs.alchemy.com/reference/getnfts)
Connect Wallet : RainbowKit http://rainbowkit.com
```
